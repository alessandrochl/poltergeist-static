#!/usr/bin/env bash

# ==================

# When using the CLI, kujo receives site-wide metadata as command line options.
# Instead of writing those options each time, you can store your options
# In a specific shell script like this one, which calls the kujo executable when run.

# ==================

# Remove the previous docs files
echo "Cleaning up..."
rm -rf docs

echo "Building..."
mkdir -p docs/images/
cp images/* docs/images/
kujo --title="Poltergeist" --description="SINCE 2018" \
--style=lit.css \
--trace --md-html --highlight --typographer --date --date-last --paginate=20 \
--views=views \
articles docs \
--site-root="/freeing-my-phone" \
--tags
